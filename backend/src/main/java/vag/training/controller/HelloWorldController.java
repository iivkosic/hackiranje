package vag.training.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {

    @GetMapping("/api/helloWorld")
    public String sayHello(){
        return "hello world";
    }

    @PostMapping("/api/calculate/{number}")
    public int calculate(@PathVariable("number") String number){
        if(!number.isEmpty()){
            int num = Integer.parseInt(number);
            return num*num;
        }
        return 0;
    }
}
