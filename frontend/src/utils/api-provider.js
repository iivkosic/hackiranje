import axios from 'axios'

export function sayHello() {
  var url = "/api/helloWorld"
  return axios.get(url)
}

export function calculate(number){
  var url = "/api/calculate/"+number
  return axios.post(url)
}

export function getEurValue() {
  var url='http://data.fixer.io/api/latest?access_key=01e33c15e8bbe737d0f4211be1259feb'
  return axios.post(url);
}
